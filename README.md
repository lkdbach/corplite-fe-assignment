This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, install the dependencies:

```bash
npm i
# or
yarn
```

after that, run:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

Some of my thoughts while working on this project:

1. I use NextJS framework to leverage the pros of this framework https://pagepro.co/blog/what-is-nextjs/
2. Chose RematchJS [https://rematchjs.org/](rematchjs) as Redux framework to reduce boilerplate code. Redux Toolkit https://redux-toolkit.js.org/ is a possible alternative. Redux Dev Tool is also enabled, that is a awesome tool to debug redux store.
3. There are so many middleware options for Redux (Redux Thunk, Redux Saga) but I don't use it in this project because RematchJS already had the same functionality. React Query is also the good solution for server side stuff, let try it.
4. I used RematchJS Immer plugin to provide ability to safely do mutable changes resulting in immutable state.
5. I used Material UI as a core UI lib because It is a true awesome UI lib for React.
6. Project structure follow the Atomic Design Methodology (https://bradfrost.com/blog/post/atomic-web-design/). In fact, there is no perfect solution that fits any possible case. This is particularly important to understand because a lot of developers are always looking for the one and only one solution to all their problems. Just pick one of them, try and enjoy.
7. There is only one remaining bonus requirement (Generate a PDF of the previewed form). I can do that if has more time.
8. We can integrate some tools like https://github.com/welldone-software/why-did-you-render to detect wasted rendering in React.
9. Husky, Eslint is also enabled to make the codebase cleaner.
10. If you have more requirements, let me know :D
