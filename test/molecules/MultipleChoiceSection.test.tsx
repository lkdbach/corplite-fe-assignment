import { fireEvent, render, screen } from '@testing-library/react'

import { TFormOptionConfig } from '~common/types'
import { MultipleChoiceSection } from '~components/molecules'

describe('MultipleChoiceSection component', () => {
  const config: Partial<TFormOptionConfig> = {
    options: [
      {
        id: 'test-1',
      },
    ],
  }
  test('render two textbox when initialized', () => {
    render(<MultipleChoiceSection config={config} />)
    const questionInputLabel = screen.queryByText('Question')
    const inputs = screen.queryAllByRole('textbox')

    expect(questionInputLabel).toBeInTheDocument()
    expect(inputs).toHaveLength(2)
  })
  test('typing in two textbox', async () => {
    render(<MultipleChoiceSection config={config} />)
    const inputs = screen.queryAllByRole('textbox')

    fireEvent.change(inputs[0], {
      target: { value: 'question textfield test #1' },
    })

    fireEvent.change(inputs[1], {
      target: { value: 'option textfield test #2' },
    })
    expect(screen.getByDisplayValue('question textfield test #1')).toBeInTheDocument()
    expect(screen.getByDisplayValue('option textfield test #2')).toBeInTheDocument()
  })
  test('add option', async () => {
    const setConfigMock = jest.fn()
    render(<MultipleChoiceSection config={config} setConfig={setConfigMock} />)

    fireEvent.click(screen.getByRole('button'))

    expect(setConfigMock).toHaveBeenCalled()
  })
  test('remove option', async () => {
    const setConfigMock = jest.fn()
    const config = {
      options: [
        {
          id: 'test-1',
        },
        {
          id: 'test-2',
        },
      ],
    }
    render(<MultipleChoiceSection config={config} setConfig={setConfigMock} />)
    const buttons = screen.getAllByText('remove_circle')

    fireEvent.click(buttons[0])

    expect(buttons).toHaveLength(2)
    expect(setConfigMock).toHaveBeenCalled()
  })
})
