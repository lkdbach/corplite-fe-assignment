import { fireEvent, render, screen } from '@testing-library/react'

import { InputSection } from '~components/molecules'

describe('InputSection component', () => {
  test('render a textfield without label', () => {
    render(<InputSection />)
    const label = screen.queryByText('testing')

    expect(screen.getByRole('textbox')).toBeInTheDocument()
    expect(label).not.toBeInTheDocument()
  })
  test('render a textfield with label', () => {
    render(<InputSection title="testing" />)

    expect(screen.getByText('testing')).toBeInTheDocument()
    expect(screen.getByRole('textbox')).toBeInTheDocument()
  })
  test('render a textfield and test input some text', () => {
    render(<InputSection />)
    fireEvent.change(screen.getByRole('textbox'), {
      target: { value: 'textfield test #1' },
    })
    expect(screen.getByDisplayValue('textfield test #1')).toBeInTheDocument()
  })
})
