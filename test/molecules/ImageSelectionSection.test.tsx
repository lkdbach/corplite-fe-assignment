import { fireEvent, render, screen } from '@testing-library/react'

import { ImageSelectionSection } from '~components/molecules'

describe('ImageSelectionSection component', () => {
  const imageOptions = [
    {
      id: '0',
      author: 'Alejandro Escamilla',
      width: 5616,
      height: 3744,
      url: 'https://unsplash.com/photos/yC-Yzbqy7PY',
      download_url: 'https://picsum.photos/id/0/5616/3744',
    },
    {
      id: '1',
      author: 'Alejandro Escamilla',
      width: 5616,
      height: 3744,
      url: 'https://unsplash.com/photos/LNRyGwIJr5c',
      download_url: 'https://picsum.photos/id/1/5616/3744',
    },
    {
      id: '10',
      author: 'Paul Jarvis',
      width: 2500,
      height: 1667,
      url: 'https://unsplash.com/photos/6J--NXulQCs',
      download_url: 'https://picsum.photos/id/10/2500/1667',
    },
    {
      id: '100',
      author: 'Tina Rataj',
      width: 2500,
      height: 1656,
      url: 'https://unsplash.com/photos/pwaaqfoMibI',
      download_url: 'https://picsum.photos/id/100/2500/1656',
    },
    {
      id: '1000',
      author: 'Lukas Budimaier',
      width: 5626,
      height: 3635,
      url: 'https://unsplash.com/photos/6cY-FvMlmkQ',
      download_url: 'https://picsum.photos/id/1000/5626/3635',
    },
  ]
  test('render 5 images when initialize', () => {
    render(<ImageSelectionSection imageOptions={imageOptions} />)
    const buttonImages = screen.queryAllByRole('button')

    expect(buttonImages).toHaveLength(5)
  })
  test('onSelect image', () => {
    const onSelectClickMock = jest.fn()
    const onSelectMock = () => onSelectClickMock

    render(<ImageSelectionSection imageOptions={imageOptions} onSelect={onSelectMock} />)
    const buttonImages = screen.queryAllByRole('button')
    fireEvent.click(buttonImages[0])

    expect(onSelectClickMock).toBeCalledTimes(1)
  })
  test('render selected image', () => {
    const selectedImages = [
      {
        id: '1000',
        author: 'Lukas Budimaier',
        width: 5626,
        height: 3635,
        url: 'https://unsplash.com/photos/6cY-FvMlmkQ',
        download_url: 'https://picsum.photos/id/1000/5626/3635',
      },
    ]

    render(<ImageSelectionSection selectedImages={selectedImages} imageOptions={imageOptions} />)
    const selectedImageOverlay = screen.queryByText('check_circle')

    expect(selectedImageOverlay).toBeInTheDocument()
  })
})
