import { createModel } from '@rematch/core'

import { TFormOptionConfig } from '~common/types'

import { RootModel } from '.'

export interface IMain {
  formTitle: string
  formDescription: string
  formOptionConfigs: TFormOptionConfig[]
}

const initState: IMain = {
  formTitle: '',
  formDescription: '',
  formOptionConfigs: [],
}

export const main = createModel<RootModel>()({
  state: initState,
  reducers: {
    update(state, payload: Partial<IMain>) {
      return { ...state, ...payload }
    },
    addFormOptionConfigs(state, payload: TFormOptionConfig) {
      state.formOptionConfigs.push(payload)
    },
    removeFormOptionConfigs(state, payload: TFormOptionConfig) {
      state.formOptionConfigs = state.formOptionConfigs.filter((config) => config.id !== payload.id)
    },
  },
})
