import { Models } from '@rematch/core'

import { main } from './main.model'

export interface RootModel extends Models<RootModel> {
  main: typeof main
}

export const models: RootModel = {
  main,
}
