import { init, RematchDispatch, RematchRootState } from '@rematch/core'
import immerPlugin from '@rematch/immer'
import { combineReducers } from 'redux'
import undoable, { StateWithHistory } from 'redux-undo'

import { models, RootModel } from './models'
import { IMain } from './models/main.model'

export const store = init<RootModel>({
  models,
  redux: {
    devtoolOptions: {
      disabled: process.env.NODE_ENV !== 'development',
    },
    combineReducers: (reducers) => {
      return combineReducers({
        ...reducers,
        main: undoable(reducers.main), // only this model should be undoable.
      })
    },
  },
  plugins: [immerPlugin()],
})

export type Store = typeof store
export type RootDispatch = RematchDispatch<RootModel>
export type RootState = RematchRootState<RootModel> & { main: StateWithHistory<IMain> }
