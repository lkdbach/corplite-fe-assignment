import { EFormOptionType } from './enums'

export type TMultipleChoiceConfigOption = {
  id: string
  option?: string
}

export type TImage = {
  author: string
  download_url: string
  height: number
  id: string
  url: string
  width: number
}

export type TFormOptionConfig = {
  id: string
  question?: string
  addOrder: number
  type: EFormOptionType
  options?: TMultipleChoiceConfigOption[]
  image?: TImage
}
