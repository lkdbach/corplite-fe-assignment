export enum EFormOptionType {
  SHORT_ANSWER = 'Short answer',
  MULTIPLE_CHOICE = 'Multiple choice',
  IMAGE_SELECTION = 'Image selection',
}
