import debounce from 'lodash/debounce'

export const useTextInputDebounce = (cb: (event: React.ChangeEvent<HTMLInputElement>) => void, delayTime = 500) => {
  return debounce(cb, delayTime)
}
