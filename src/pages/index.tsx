import type { GetServerSideProps, NextPage } from 'next'
import { resetServerContext } from 'react-beautiful-dnd'

import { MainPage } from '~components/pages'

const Home: NextPage = () => {
  return <MainPage />
}

export const getServerSideProps: GetServerSideProps = async () => {
  resetServerContext() // <-- CALL RESET SERVER CONTEXT, SERVER SIDE

  return { props: { data: [] } }
}

export default Home
