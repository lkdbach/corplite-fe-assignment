import { Box, TextField, Typography } from '@mui/material'
import React from 'react'

import { useTextInputDebounce } from '~hooks/useCommon'
import { FCC } from '~types'

type Props = {
  title?: string
  onChange?: React.ChangeEventHandler<HTMLInputElement | HTMLTextAreaElement>
  fullWidth?: boolean
}

const InputSection: FCC<Props> = ({ title = '', onChange = () => null, fullWidth = false }) => {
  const onChangeDebounce = useTextInputDebounce(onChange)
  return title ? (
    <Box display="flex" flexDirection="column" gap={2}>
      <Typography variant="body1">
        <b>{title}</b>
      </Typography>
      <TextField
        sx={{ width: fullWidth ? '100%' : { xs: '100%', md: '70%' } }}
        id={title?.toLowerCase()?.replace(' ', '-') || ''}
        variant="outlined"
        size="small"
        onChange={onChangeDebounce}
      />
    </Box>
  ) : (
    <TextField
      sx={{ width: fullWidth ? '100%' : { xs: '100%', md: '70%' } }}
      variant="outlined"
      size="small"
      onChange={onChangeDebounce}
    />
  )
}

export default InputSection
