import { Box, Checkbox, Icon, IconButton, Tooltip } from '@mui/material'
import { uniqueId } from 'lodash'
import React from 'react'

import { TFormOptionConfig, TMultipleChoiceConfigOption } from '~common/types'
import { FCC } from '~types'

import InputSection from './InputSection'

type Props = {
  config: Partial<TFormOptionConfig>
  setConfig?: React.Dispatch<React.SetStateAction<Partial<TFormOptionConfig>>>
}

const MultipleChoiceSection: FCC<Props> = ({ config, setConfig = () => null }) => {
  const onChange = (key: keyof TFormOptionConfig) => (event: React.ChangeEvent<HTMLInputElement>) => {
    setConfig((prevState) => ({ ...prevState, [key]: event.target.value }))
  }

  const onChangeOption =
    (changeOption: TMultipleChoiceConfigOption) => (event: React.ChangeEvent<HTMLInputElement>) => {
      setConfig((prevState) => ({
        ...prevState,
        options: prevState?.options?.map((originOption) =>
          originOption.id === changeOption.id ? { ...originOption, option: event.target.value } : originOption,
        ),
      }))
    }

  const addOption = () => {
    setConfig((prevState) => ({
      ...prevState,
      options: [...(prevState.options as Array<TMultipleChoiceConfigOption>), { id: uniqueId('multiple_choice') }],
    }))
  }

  const removeOption = (removeOption: TMultipleChoiceConfigOption) => () => {
    setConfig((prevState) => ({
      ...prevState,
      options: (prevState.options as Array<TMultipleChoiceConfigOption>).filter(
        (option) => option.id !== removeOption.id,
      ),
    }))
  }

  return (
    <Box display="flex" flexDirection="column" gap={4}>
      <InputSection title="Question" onChange={onChange('question')} fullWidth />
      {config?.options?.map((optionConfig) => {
        return (
          <Box key={optionConfig.id} display="flex" flexDirection="row" gap={2}>
            <Checkbox defaultChecked disabled />
            <InputSection fullWidth onChange={onChangeOption(optionConfig)} />
            <Box display="flex" flexDirection="row" gap={2}>
              {config?.options && config?.options.length > 1 && (
                <Tooltip title="Remove option">
                  <IconButton color="error" onClick={removeOption(optionConfig)}>
                    <Icon>remove_circle</Icon>
                  </IconButton>
                </Tooltip>
              )}
              <Tooltip title="Add option">
                <IconButton color="primary" onClick={addOption}>
                  <Icon>add_circle</Icon>
                </IconButton>
              </Tooltip>
            </Box>
          </Box>
        )
      })}
    </Box>
  )
}

export default MultipleChoiceSection
