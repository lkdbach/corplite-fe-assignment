/* eslint-disable react/display-name */
/* eslint-disable @next/next/no-img-element */
import { Box, Icon, IconButton, Typography } from '@mui/material'

import { TImage } from '~common/types'
import { FCC } from '~types'

type Props = {
  onSelect?: (selectedImage: TImage) => () => void
  selectedImages?: TImage[]
  imageOptions: TImage[]
}

const ImageSelectionSection: FCC<Props> = ({ onSelect = () => () => null, selectedImages = [], imageOptions }) => {
  return (
    <Box display="flex" flexDirection="column" gap={4}>
      <Typography variant="h6">Select images</Typography>
      <Box display="flex" flexDirection="row" flexWrap="wrap">
        {imageOptions.map((option) => (
          <IconButton
            key={option.id}
            sx={{ width: 'calc(100% / 3 - 8px)', padding: 0, margin: 0.5, position: 'relative' }}
            onClick={onSelect(option)}
          >
            <img src={option.download_url} alt="image-selection" style={{ width: '100%', height: 'auto' }} />
            {selectedImages.find((selectedImage) => selectedImage.id === option.id) && (
              <Box
                sx={{
                  background: '#6e6b6b66',
                  position: 'absolute',
                  top: 0,
                  left: 0,
                  right: 0,
                  bottom: 0,
                  display: 'flex',
                  justifyContent: 'flex-end',
                }}
              >
                <Icon color="primary" sx={{ margin: 1 }}>
                  check_circle
                </Icon>
              </Box>
            )}
          </IconButton>
        ))}
      </Box>
    </Box>
  )
}

export default ImageSelectionSection
