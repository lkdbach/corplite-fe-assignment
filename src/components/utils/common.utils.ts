import { TImage } from '~common/types'

export const preloadAndCacheImages = async (imageOptions: TImage[]) => {
  // preload and cache image
  const promises = imageOptions.map((imageOption) => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    return new Promise((resolve: any, reject: any) => {
      const img = new Image()
      img.src = imageOption.download_url

      img.onload = resolve()
      img.onerror = reject()
    })
  })
  await Promise.all(promises)
}

export const reorder = (list: Array<unknown>, startIndex: number, endIndex: number) => {
  const result = Array.from(list)
  const [removed] = result.splice(startIndex, 1)
  result.splice(endIndex, 0, removed)

  return result
}
