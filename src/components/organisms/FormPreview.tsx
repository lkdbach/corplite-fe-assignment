import { Box, Checkbox, Icon, IconButton, Typography } from '@mui/material'
import { DragDropContext, Draggable, Droppable, DropResult } from 'react-beautiful-dnd'
import { useSelector } from 'react-redux'
import { ActionCreators } from 'redux-undo'

import { EFormOptionType } from '~common/enums'
import { TFormOptionConfig } from '~common/types'
import { InputSection } from '~components/molecules'
import { reorder } from '~components/utils/common.utils'
import { useRematchDispatch } from '~hooks/useRematchDispatch'
import { RootDispatch, RootState } from '~stores'
import { FCC } from '~types'

const FormPreview: FCC = () => {
  const { formDescription, formTitle, formOptionConfigs } = useSelector((state: RootState) => state.main.present)
  const { removeFormOptionConfigs, update, dispatch } = useRematchDispatch((dispatch: RootDispatch) => ({
    removeFormOptionConfigs: dispatch.main.removeFormOptionConfigs,
    update: dispatch.main.update,
    dispatch,
  }))

  const onRemoveFormOptionConfigs = (formOptionConfig: TFormOptionConfig) => () => {
    removeFormOptionConfigs(formOptionConfig)
  }

  const handleUndo = () => dispatch(ActionCreators.undo())
  const handleRedo = () => dispatch(ActionCreators.redo())

  const onDragEnd = (result: DropResult) => {
    // dropped outside the list
    if (!result.destination) {
      return
    }

    const reorderedFormOptionConfigs = reorder(formOptionConfigs, result.source.index, result.destination.index)
    update({ formOptionConfigs: reorderedFormOptionConfigs as TFormOptionConfig[] })
  }

  return (
    <Box display="flex" flexDirection="column" flex={1}>
      <Box display="flex" flexDirection="row" justifyContent={'space-between'} alignItems="center">
        <Typography>
          <b>Form Preview</b>
        </Typography>
        <Box display="flex" flexDirection="row" justifyContent={'space-between'}>
          <IconButton color="primary" onClick={handleUndo}>
            <Icon>undo_rounded</Icon>
          </IconButton>
          <IconButton color="primary" onClick={handleRedo}>
            <Icon>redo_rounded</Icon>
          </IconButton>
        </Box>
      </Box>

      <DragDropContext onDragEnd={onDragEnd}>
        <Droppable droppableId="droppable">
          {(provided, snapshot) => (
            <div
              {...provided.droppableProps}
              ref={provided.innerRef}
              style={{
                display: 'flex',
                flexDirection: 'column',
                minHeight: 800,
                borderRadius: 8,
                marginTop: 16,
                border: '1px solid black',
                padding: 8,
                flex: 1,
                background: snapshot.isDraggingOver ? 'lightblue' : 'transparent',
              }}
            >
              <Box display="flex" flexDirection="column" flex={1} gap={4}>
                {formTitle && (
                  <Typography variant="h6">
                    <b>{formTitle}</b>
                  </Typography>
                )}
                {formDescription && <Typography variant="body2">{formDescription}</Typography>}
                {formOptionConfigs?.length > 0 &&
                  formOptionConfigs.map((config, index) => {
                    switch (config.type) {
                      case EFormOptionType.SHORT_ANSWER:
                        return (
                          <Draggable key={config.id} draggableId={config.id} index={index}>
                            {(provided, snapshot) => (
                              <div ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
                                <Box
                                  display="flex"
                                  flexDirection={'column'}
                                  sx={{
                                    position: 'relative',
                                    background: snapshot.isDragging ? 'lightgreen' : 'transparent',
                                  }}
                                >
                                  <InputSection title={config.question} fullWidth />
                                  <IconButton
                                    onClick={onRemoveFormOptionConfigs(config)}
                                    sx={{ position: 'absolute', padding: 0, right: 0 }}
                                  >
                                    <Icon>close</Icon>
                                  </IconButton>
                                </Box>
                              </div>
                            )}
                          </Draggable>
                        )
                      case EFormOptionType.MULTIPLE_CHOICE:
                        return (
                          <Draggable key={config.id} draggableId={config.id} index={index}>
                            {(provided, snapshot) => (
                              <div ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
                                <Box
                                  display="flex"
                                  flexDirection={'column'}
                                  key={config.id}
                                  sx={{
                                    position: 'relative',
                                    background: snapshot.isDragging ? 'lightgreen' : 'transparent',
                                  }}
                                  gap={1}
                                >
                                  <Typography variant="body1">
                                    <b>{config.question}</b>
                                  </Typography>
                                  {config?.options?.map((optionConfig) => {
                                    return (
                                      <Box
                                        ml={2}
                                        key={optionConfig.id}
                                        display="flex"
                                        flexDirection="row"
                                        gap={2}
                                        alignItems="center"
                                      >
                                        <Checkbox />
                                        <Typography variant="body2">{optionConfig.option}</Typography>
                                      </Box>
                                    )
                                  })}
                                  <IconButton
                                    onClick={onRemoveFormOptionConfigs(config)}
                                    sx={{ position: 'absolute', padding: 0, right: 0 }}
                                  >
                                    <Icon>close</Icon>
                                  </IconButton>
                                </Box>
                              </div>
                            )}
                          </Draggable>
                        )
                      case EFormOptionType.IMAGE_SELECTION:
                        return (
                          <Draggable key={config.id} draggableId={config.id} index={index}>
                            {(provided) => (
                              <div ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
                                <Box display="flex" justifyContent="center" position="relative">
                                  <Box
                                    component={'img'}
                                    src={config.image?.download_url}
                                    alt={'selected-image'}
                                    width="100%"
                                  />
                                  <IconButton
                                    onClick={onRemoveFormOptionConfigs(config)}
                                    sx={{ position: 'absolute', padding: 0, right: 0 }}
                                  >
                                    <Icon sx={{ margin: 1 }} color="primary">
                                      cancel_rounded
                                    </Icon>
                                  </IconButton>
                                </Box>
                              </div>
                            )}
                          </Draggable>
                        )
                    }
                  })}
              </Box>
              {provided.placeholder}
            </div>
          )}
        </Droppable>
      </DragDropContext>
    </Box>
  )
}

export default FormPreview
