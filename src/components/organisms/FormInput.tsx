import { Box, Button, FormControl, InputLabel, MenuItem, Select, SelectChangeEvent } from '@mui/material'
import { uniqueId } from 'lodash'
import React, { useCallback, useEffect, useState } from 'react'

import { EFormOptionType } from '~common/enums'
import { TFormOptionConfig, TImage } from '~common/types'
import { InputSection, MultipleChoiceSection } from '~components/molecules'
import ImageSelectionSection from '~components/molecules/ImageSelectionSection'
import { preloadAndCacheImages } from '~components/utils/common.utils'
import { useRematchDispatch } from '~hooks/useRematchDispatch'
import { RootDispatch } from '~stores'
import { FCC } from '~types'

let addFormOptionOrder = 0

const FormInput: FCC = () => {
  const [formOptionType, setFormOptionType] = useState<EFormOptionType | null>(null)
  const [tmpShortAnswerConfig, setTmpShortAnswerConfig] = useState<Partial<TFormOptionConfig> | null>(null)
  const [tmpMultipleChoiceConfig, setTmpMultipleChoiceConfig] = useState<Partial<TFormOptionConfig>>({
    options: [
      {
        id: uniqueId('multiple_choice'),
      },
    ],
  })
  const [imageOptions, setImageOptions] = useState<TImage[]>([])
  const [tmpSelectedImages, setTmpSelectedImages] = useState<TImage[]>([])
  const { updateMain, addFormOptionConfigs } = useRematchDispatch((dispatch: RootDispatch) => ({
    updateMain: dispatch.main.update,
    addFormOptionConfigs: dispatch.main.addFormOptionConfigs,
  }))

  const getImages = useCallback(async () => {
    const res = await fetch('https://picsum.photos/v2/list?limit=5').then((response) => response.json())
    preloadAndCacheImages(res as TImage[])
    setImageOptions(res as TImage[])
    console.log(JSON.stringify(res))
  }, [])

  useEffect(() => {
    getImages()
  }, [getImages])

  const onFormTitleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    updateMain({ formTitle: event.target.value })
  }

  const onFormDescriptionChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    updateMain({ formDescription: event.target.value })
  }

  const onFormOptionTypeChange = (event: SelectChangeEvent) => {
    setFormOptionType(event.target.value as EFormOptionType)
    setTmpShortAnswerConfig(null)
    setTmpMultipleChoiceConfig({
      options: [
        {
          id: uniqueId('multiple_choice'),
        },
      ],
    })
    setTmpSelectedImages([])
  }

  const addFormOption = (formOptionType: EFormOptionType) => () => {
    addFormOptionOrder++
    switch (formOptionType) {
      case EFormOptionType.SHORT_ANSWER:
        addFormOptionConfigs({
          ...tmpShortAnswerConfig,
          id: uniqueId('form_short_answer'),
          type: EFormOptionType.SHORT_ANSWER,
          addOrder: addFormOptionOrder,
        } as TFormOptionConfig)
        break
      case EFormOptionType.MULTIPLE_CHOICE:
        addFormOptionConfigs({
          ...tmpMultipleChoiceConfig,
          id: uniqueId('form_multiple_choice'),
          type: EFormOptionType.MULTIPLE_CHOICE,
          addOrder: addFormOptionOrder,
        } as TFormOptionConfig)
        break
      case EFormOptionType.IMAGE_SELECTION:
        tmpSelectedImages.forEach((image) => {
          addFormOptionConfigs({
            image: image,
            id: uniqueId('form_image-selection'),
            type: EFormOptionType.IMAGE_SELECTION,
            addOrder: addFormOptionOrder,
          } as TFormOptionConfig)
          addFormOptionOrder++
        })
        setTmpSelectedImages([])
        break
    }
  }

  const onShortAnswerConfigChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setTmpShortAnswerConfig({ question: event.target.value })
  }

  const toggleSelectImage = (selectedImage: TImage) => () => {
    setTmpSelectedImages((prevState) => {
      const exists = !!prevState.find((imageOption) => imageOption.id === selectedImage.id)
      if (exists) {
        return prevState.filter((imageOption) => imageOption.id !== selectedImage.id)
      }
      return [...prevState, selectedImage]
    })
  }

  return (
    <Box display="flex" gap={4} flexDirection="column" flex={1}>
      <InputSection title="Form Title" onChange={onFormTitleChange} />
      <InputSection title="Form Description" onChange={onFormDescriptionChange} />
      <FormControl sx={{ width: { xs: '100%', md: '70%' } }}>
        <InputLabel id="select-form-options-label">Add Option Type</InputLabel>
        <Select
          labelId="select-form-options-label"
          id="select-form-options"
          value={formOptionType ?? ''}
          label="Add Option Type"
          onChange={onFormOptionTypeChange}
        >
          <MenuItem value={EFormOptionType.SHORT_ANSWER}>{EFormOptionType.SHORT_ANSWER}</MenuItem>
          <MenuItem value={EFormOptionType.MULTIPLE_CHOICE}>{EFormOptionType.MULTIPLE_CHOICE}</MenuItem>
          <MenuItem value={EFormOptionType.IMAGE_SELECTION}>{EFormOptionType.IMAGE_SELECTION}</MenuItem>
        </Select>
      </FormControl>
      <Box
        display="flex"
        flexDirection="column"
        border="1px solid black"
        borderRadius={2}
        flex={1}
        p={2}
        minHeight={400}
      >
        {formOptionType === EFormOptionType.SHORT_ANSWER && (
          <InputSection title="Question" onChange={onShortAnswerConfigChange} fullWidth />
        )}
        {formOptionType === EFormOptionType.MULTIPLE_CHOICE && (
          <MultipleChoiceSection config={tmpMultipleChoiceConfig} setConfig={setTmpMultipleChoiceConfig} />
        )}
        {formOptionType === EFormOptionType.IMAGE_SELECTION && (
          <ImageSelectionSection
            imageOptions={imageOptions}
            selectedImages={tmpSelectedImages}
            onSelect={toggleSelectImage}
          />
        )}
      </Box>
      <Box display="flex" flexDirection="column">
        <Button variant="contained" onClick={addFormOption(formOptionType as EFormOptionType)}>
          Add
        </Button>
      </Box>
    </Box>
  )
}

export default FormInput
