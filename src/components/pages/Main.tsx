import { Box, Grid, Typography } from '@mui/material'

import { MainLayout } from '~components/layouts'
import { FormInput, FormPreview } from '~components/organisms'
import { FCC } from '~types'

const Main: FCC = () => {
  return (
    <MainLayout>
      <Box sx={{ padding: { xs: 2, md: 0 }, display: 'flex', flexDirection: 'column', gap: 2, flex: 1 }}>
        <Typography variant="h5">
          <b>Form Creator</b>
        </Typography>
        <Grid container spacing={5} display="flex" flex={1}>
          <Grid item xs={12} md={6} display="flex" flexDirection="column" maxHeight={'93vh'}>
            <FormInput />
          </Grid>
          <Grid item xs={12} md={6} display="flex" flexDirection="column" mt={{ xs: 5, md: 0 }}>
            <FormPreview />
          </Grid>
        </Grid>
      </Box>
    </MainLayout>
  )
}

export default Main
