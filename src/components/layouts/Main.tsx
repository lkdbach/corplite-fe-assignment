import { Box } from '@mui/material'
import React from 'react'

import { FCC } from '~types'

const Main: FCC = ({ children }) => {
  return (
    <Box display="flex" justifyContent="center">
      <Box display="flex" flex={1} maxWidth={1440} minHeight={'100vh'} paddingY={{ xs: 0, xl: 5 }}>
        {children}
      </Box>
    </Box>
  )
}

export default Main
